import numpy as np


def velocity_verlet(planet_coord, planet_vel, sun_mass):
    planet_sun_distance = np.sqrt(np.sum(np.square(planet_coord)))
    planet_sun_direction = -planet_coord/planet_sun_distance
    acceleration_t = (sun_mass/planet_sun_distance**2)*planet_sun_direction

    # update with acceleration at t
    planet_coord += planet_vel + 0.5*acceleration_t

    planet_sun_distance = np.sqrt(np.sum(np.square(planet_coord)))
    planet_sun_direction = -planet_coord/planet_sun_distance
    acceleration_tp1 = (sun_mass/planet_sun_distance**2)*planet_sun_direction

    # update with acceleration at t and t+1
    planet_vel += 0.5*(acceleration_t + acceleration_tp1)
    return planet_coord, planet_vel
