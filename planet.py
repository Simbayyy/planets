import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.colors as mcolors


from planet_modules import velocity_verlet
from planet_generator import create_planet

# setup the figure
fig, ax = plt.subplots()
ax.set_xlim(-1, 1)
ax.set_ylim(-1, 1)
ax.set_aspect("equal")

ax.plot([0], [0], "o", ms=30, c="gold")


velocity_magnitude = 0.01
sun_mass = 0.0002


def make_many_planets(N=1):
    planets = []
    for _ in range(N):
        planets.append(tuple(create_planet(velocity_magnitude)))
    return planets


planets = make_many_planets()

planet_coord = planets[0][0]
planet_vel = planets[0][1]
planet_color = planets[0][2]


(line1,) = ax.plot([], [], marker=".", color=planet_color, ms=20)


def animate(i, planet_coord, planet_vel, sun_mass):

    planet_coord, planet_vel = \
        velocity_verlet(planet_coord, planet_vel, sun_mass)
    line1.set_data(planet_coord[0], planet_coord[1])  # update the data
    return (line1,)


ani = animation.FuncAnimation(
    fig,
    animate,
    fargs=(planet_coord, planet_vel, sun_mass),
    interval=2,
    blit=True,
)
# %%
