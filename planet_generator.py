import numpy as np
import matplotlib.colors as mcolor


def create_planet(velocity_magnitude):
    """Return coordinates, velocity, and color chosen randomly for a planet

    Parameters
    ----------

    velocity_magnitude -- float
        the magnitude of the velocity  
    
    Returns
    -------

    planet_coord -- np.NDArray, float64, (1,2), 
        two random floats between -1 and 1

    planet_vel -- 

    """

    planet_coord = np.asarray(2*np.random.random(2)-1)
    x_velocity = (2*np.random.random()-1)*velocity_magnitude
    y_velocity = np.random.choice([-1, 1]) * \
        np.sqrt(velocity_magnitude**2 - x_velocity**2)
    planet_vel = np.asarray([x_velocity, y_velocity])
    planet_color = np.random.choice(list(mcolor.CSS4_COLORS.values()))
    return planet_coord, planet_vel, planet_color
